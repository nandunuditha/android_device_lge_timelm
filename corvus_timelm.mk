#
# Copyright (C) 2022 The CorvusOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# TODO: Add this file.
# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit from timelm device
$(call inherit-product, device/lge/timelm/device.mk)

# Inherit some common Corvus-OS stuff.
$(call inherit-product, vendor/corvus/config/common_full_phone.mk)

# Corvus official stuff
RAVEN_LAIR := Official
PRODUCT_PROPERTY_OVERRIDES += \
    ro.corvus.maintainer=nandunuditha
CORVUS_MAINTAINER := nandunuditha

# proton-clang
USE_PROTON := true

# Gapps
TARGET_GAPPS_ARCH := arm64
USE_GAPPS := true

TARGET_FACE_UNLOCK_SUPPORTED := true
scr_resolution := 1080

PRODUCT_NAME := corvus_timelm
PRODUCT_DEVICE := timelm
PRODUCT_BRAND := lge
PRODUCT_MODEL := LM-V600
PRODUCT_MANUFACTURER := LGE
PRODUCT_RELEASE_NAME := V60 ThinQ
PRODUCT_SYSTEM_NAME := LGV60
PRODUCT_SYSTEM_DEVICE := LGV60

PRODUCT_GMS_CLIENTID_BASE := android-lge

PRODUCT_BUILD_PROP_OVERRIDES += \
PRIVATE_BUILD_DESC="timelm-user 12/SKQ1.211103.001/222851743aa2c release-keys"

PRODUCT_DEFAULT_PROPERTY_OVERRIDE += \
ro.adb.secure=0

BUILD_FINGERPRINT := "lge/timelm/timelm:12/SKQ1.211103.001/222851743aa2c:user/release-keys"