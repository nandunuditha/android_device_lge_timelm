PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/corvus_timelm.mk

COMMON_LUNCH_CHOICES := \
    corvus_timelm-user \
    corvus_timelm-userdebug \
    corvus_timelm-eng
